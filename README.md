# deadman
**kernel module deadman-switch**

Deadman is linux kernel module deadman-switch that responds to removal of a specified USB device or the insertion of any.

## Rationale
When information on the storage of the computer is encrypted at-rest, it is still accessible when the machine is in use, unencrypted. Only when the hardware gets turned off does the encryption fully protect the data, and when a computer is seized by a competent agent while running, unencrypted information could be accessed.

If the computer can be made to turn off in an emergency, it will heightens the security. A deadman USB device only needs to be removed to trigger a power-off, and when it is strapped to ones wrist, it will be harder to prevent the removal when attacked. Another attack is insertion of a USB device into a running system, and this can also be made to trigger a power-off.

## Build & Use
* Required: `make linux-headers gcc`
* Build: `make`
* Install: `sudo make install`
* Usage (built but not installed): `sudo insmod deadman.ko [id=0xVENDPROD] [off=0] [ins=1] [log=0]`
* Usage (if installed): `sudo modprobe deadman [id=0xVENDPROD] [off=0] [ins=1] [log=0]`
  - `id`: *VEND* is the 2-byte hexadecimal code for idVendor and *PROD* for idProduct. The `id` parameter can also be set to a default device in the source before building.
  - `off`: if `off=0` is given, the computer will not turn off on a panic (`off=1` is the default).
  - `ins`: if `ins=1` is given, any USB device getting inserted triggers a panic (`ins=0` is the default).
  - `log`: if `log=0` is given, no messages will be logged anywhere (`log=1` is the default).

